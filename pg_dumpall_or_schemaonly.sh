#!/bin/bash
#
#
#
#
#

# Script Informations
PROGNAME="$(basename $0)"
PROGPATH=$(dirname $(readlink -f $0))
ROOTDIR="$PROGPATH/.."

# ALL DUMP OR ONLY LIST
ENABLE_DUMPALL=no
SCHEMA_ONLY_LIST="database1,database2"

# BACKUP DIR
BACKUP_DIR=/backups

# PostgreSQL Informations (username & hostname)
USERNAME="postgres"
HOSTNAME="127.0.0.1"

timestamp=$(date '+%y%m%d-%H%M')

if ! mkdir -p $BACKUP_DIR; then
	echo "Cannot create backup directory in $BACKUP_DIR. Go and fix it!" 1>&2
	exit 1;
fi;

###################################
# Backup All PostgreSQL Databases #
###################################
if [ ${ENABLE_DUMPALL} = "yes" ]
then
  set -o pipefail
  if ! pg_dumpall -h "${HOSTNAME}" -U "${USERNAME}" | gzip > ${BACKUP_DIR}/all-databases.sql.gz.in_progress; then
    echo "[!!ERROR!!] Failed to produce globals backup" 1>&2
  else
    mv ${BACKUP_DIR}/all-databases.sql.gz.in_progress ${BACKUP_DIR}/all-databases-${timestamp}.sql.gz
  fi
    set +o pipefail
else
  ###################################
  # SCHEMA-ONLY BACKUPS             #
  ###################################
  for SCHEMA_ONLY_DB in ${SCHEMA_ONLY_LIST//,/ }
  do
    SCHEMA_ONLY_CLAUSE="${SCHEMA_ONLY_CLAUSE} or datname ~ '${SCHEMA_ONLY_DB}'"
  done

  SCHEMA_ONLY_QUERY="select datname from pg_database where false ${SCHEMA_ONLY_CLAUSE} order by datname;"
  SCHEMA_ONLY_DB_LIST=$(psql -h "${HOSTNAME}" -U "${USERNAME}" -At -c "${SCHEMA_ONLY_QUERY}" postgres)

  for DATABASE in ${SCHEMA_ONLY_DB_LIST}
  do
    echo "Schema-only backup of ${DATABASE}"

    set -o pipefail
      if ! pg_dump -Fp -s -h "${HOSTNAME}" -U "${USERNAME}" "${DATABASE}" | gzip > ${BACKUP_DIR}/"${DATABASE}".sql.gz.in_progress; then
      	echo "[!!ERROR!!] Failed to backup database schema of $DATABASE" 1>&2
      else
      	mv ${BACKUP_DIR}/${DATABASE}.sql.gz.in_progress ${BACKUP_DIR}/${DATABASE}-${timestamp}.sql.gz
      fi
	set +o pipefail
  done
fi

