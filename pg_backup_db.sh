#!/bin/bash

#####################################################################################################
#                                                                                                   #
# Script Name     : backup-postgresql.sh                                                            #
# Description     : This script will create a backup file of a PostgreSQL database and compress it. #
# Company         : ${COMPANY}                                                                      #
# Last updated on : 2023/06/21                                                                      #
# License         : GPL (General Public License)                                                    #
#####################################################################################################

###################################
# Global Variables                #
###################################
#
PROGNAME="$(basename $0)"
PROGPATH=$(dirname $(readlink -f $0))
ROOTDIR="$PROGPATH/.."

timestamp_bkp=$(date '+%Y%m%d-%H%M%S')

LOGPATH="$PROGPATH/log"
LOGFILE="$LOGPATH/backup-$(date '+%Y%m%d').log"

###################################
# POSTGRESQL BACKUP CONFIG        #
###################################
#
# Optional hostname. Will default to "127.0.0.1" if none specified.
# The IP address must be in IPv4 format with four numbers ranging from 0 to 255, separated by periods. Otherwise, the script will stop.
POSTGRES_IP=""

# Optional port. Will default to "5432" if none specified.
POSTGRES_PORT=""

# Optional system user to run backups. Will default to "postgres" if none specified.
# The user should be able to connect to the database without a password and with at least the IP address provided as an argument.
POSTGRES_USER=""

#The list of databases to be backed up, separated by commas.
POSTGRES_DATABASES=""

# The backup directory must exist; otherwise, the script will stop.
POSTGRES_BACKUPDIR=""

###################################
# Usage && Help                   #
###################################
#
print_usage() {
  echo "Usage: $PROGNAME [-h] || [-i] <ip_address> [-p] <port> [-u] <username> [-d] <database1,databases2> [-s] <stockage>"
}

print_help() {
  echo ""
  echo
  print_usage
  echo
  echo "-h    Display this help."
  echo "-i    Specify the IP address of the PostgreSQL server - (Optional, default : 127.0.0.1)."
  echo "-p    Specify the listening port of PostgreSQL - (Optional, default : 5432)."
  echo "-u    Specify the user used for the backups - (Optional, default postgres)."
  echo "-d    Specify the list of databases to be backed up - Required."
  echo "-s    Specify the backup directory - Required."
}

###################################
# functions                       #
###################################
#
# This function is used to trace all events.
#
log() {
  local log_file="$LOGFILE"

  if ! mkdir -p $LOGPATH; then
    echo "Cannot create log directory $LOGPATH. Go and fix it!" 1>&2
    exit 1;
  fi;

  if ! touch -c "$LOGFILE"; then
    echo "Cannot create log file $LOGFILE. Go and fix it!" 1>&2
    exit 1;
  fi;

  local timestamp=$(date +"%Y-%m-%d %T")
  echo "[$timestamp] $1" >> "$log_file"
}

#
# This function is used to verify that the IP address format is correct.
#
function check_ipaddress() {
  local IpAddress=$1
  local pattern="^([0-9]{1,3}\.){3}[0-9]{1,3}$"

  if [[ ${IpAddress} =~ ${pattern} ]]; then
    IFS='.' read -ra octets <<< "${IpAddress}"
    for octet in "${octets[@]}"; do
      if ((octet > 255)); then
        return 1
      fi
    done

    return 0
  else
    return 1
  fi
}

#
# This function is used to verify that the database connection is operational.
#
check_postgresql_connection() {
  hostname="${1}"
  port="${2}"
  username="${3}"
  database="${4}"

  psql -h "${hostname}" -p "${port}" -U "${username}" -d "${database}" -c "SELECT 1" >/dev/null 2>&1
  if [ $? -ne 0 ]; then
    log "Connection attempt fails. Check whether the connection works with psql."
    exit 1
  fi
}

#
# This function is used to backup the database(s).
#
function backup_db() {
  check_postgresql_connection "${POSTGRES_IP}" "${POSTGRES_PORT}" "$POSTGRES_USER" "postgres"

  local POSTGRES_DATABASES="${4}"

  for DATABASE in ${POSTGRES_DATABASES//,/ }
  do
    DATABASE_CLAUSE="${DATABASE_CLAUSE} or datname = '${DATABASE}'"
  done

  DATABASE_QUERY="select datname from pg_database where false ${DATABASE_CLAUSE} order by datname;"
  DATABASE_LIST=$(psql -h "${1}" -p "${2}" -U "${3}" -w -At -c "${DATABASE_QUERY}" postgres)

  for DATABASE in ${DATABASE_LIST}
  do
    log "##### Starting backup (DB: ${DATABASE}) #####"
    start_time=$(date +%s)

    log "hostname: ${1}, port: ${2}, username: ${3}, database:${DATABASE}, stockage: ${5}"
    
    set -o pipefail
      if ! pg_dump -h "${1}" -p "${2}" -U "${3}" -w -d "${DATABASE}" | gzip > ${5}/"${DATABASE}".sql.gz.in_progress; then
        log "[!!ERROR!!] Failed to backup database schema - $DATABASE"
        exit 1
      else
        mv ${5}/${DATABASE}.sql.gz.in_progress ${5}/${DATABASE}-${timestamp_bkp}.sql.gz
      fi
    set +o pipefail

    end_time=$(date +%s)
    duration=$((end_time - start_time))
    execution_time=$(date -d@${duration} -u +%H:%M:%S)

    log "##### Database backup completed in ${execution_time} seconds. #####"
  done
}

###################################
# Retrieving the arguments        #
###################################
#
while getopts hi:p:u:d:s: OPTION; do
  case $OPTION in
    h) print_help
       exit 0
       ;;
    i) POSTGRES_IP=$OPTARG
       ;;
    p) POSTGRES_PORT=$OPTARG
       ;;
    u) POSTGRES_USER=$OPTARG
       ;;
    d) POSTGRES_DATABASES=$OPTARG
       ;;
    s) POSTGRES_BACKUPDIR=$OPTARG
       ;;
    *) print_usage
       exit 1
       ;;
  esac
done
shift $(($OPTIND - 1))

###################################
# PRE-BACKUP CHECKS               #
###################################
#
if ! test -z "${POSTGRES_IP}" && ! check_ipaddress "${POSTGRES_IP}"; then
  log "The format of the IP address is incorrect."
  exit 1
fi

if [ -n "${POSTGRES_BACKUPDIR}" ] && [ ! -d "${POSTGRES_BACKUPDIR}" ]; then
  log "The backup directory does not exist."
  exit 1
fi

###################################
# INITIALISE DEFAULTS             #
###################################
#
if [ -z "${POSTGRES_IP}" ]; then
  POSTGRES_IP="127.0.0.1"
fi

if [ -z "$POSTGRES_PORT" ]; then
  POSTGRES_PORT="5432"
fi

if [ -z "$POSTGRES_USER" ]; then
  POSTGRES_USER="postgres"
fi

if [ -z "$POSTGRES_DATABASES" ]; then
  POSTGRES_DATABASES="postgres"
fi

###################################
# START THE BACKUPS               #
###################################
#
backup_db "${POSTGRES_IP}" "${POSTGRES_PORT}" "$POSTGRES_USER" "$POSTGRES_DATABASES" "${POSTGRES_BACKUPDIR}"

